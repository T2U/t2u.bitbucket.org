# TU² Front WebSite, 

Copyright (C) 2014-2015, TU² (ISITECH Student team).

------------------------------------------------------------------------
------------------------------------------------------------------------

This software is currently maintained by,

* @alandrieu
* @Gappy

A part of TU² Team (ISITECH Student team).

Introduction
------------
TU² WebSite, is a support of TU² Team. We represent ISITECH developing school.

More information about our project on :

* [Server Overview](https://bitbucket.org/T2U/t2u_server)
* [Client Overview](https://bitbucket.org/T2U/t2u_client)

Quick Start
-----------

* [Server Doc](https://bitbucket.org/T2U/t2u_server)
* [Client Doc](https://bitbucket.org/T2U/t2u_client)

License
------------

The MIT License

Copyright (c) 2014-2015 T2U Student team, composed of Alexis Landrieu and Guillaume Appy. https://bitbucket.org/T2U

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-----------
Sources
##External Tools
###Sources
* [Bitbucket](https://bitbucket.org/T2U)
###Continuus integration 
* [AppVeyor](https://ci.appveyor.com/projects)
* [Wercker](https://app.wercker.com/sessions/new)
###Organisation
* [Producteev](https://www.producteev.com)
* [Hipchat](https://t2u.hipchat.com)
###Development Tools
* [Openshift](https://www.openshift.com/)
* [IntelliJ IDEA](https://www.jetbrains.com/idea)
###Web Development Tools
* [GTmetrix](https://gtmetrix.com)
* [CloudFlare](https://www.cloudflare.com)
